﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library_Epam.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(TestData.Data.Articles);
        }
    }
}