﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Library_Epam.Helpers
{
    public static class ListHelper
    {

        public static MvcHtmlString CreateListExtension(this HtmlHelper html, string[] items)
        {
            TagBuilder ol = new TagBuilder("ol");
            foreach (var item in items)
            {
                TagBuilder li = new TagBuilder("li");
                li.SetInnerText(item);
                ol.InnerHtml += li.ToString();
            }

            return new MvcHtmlString(ol.ToString());
        }
    }
}